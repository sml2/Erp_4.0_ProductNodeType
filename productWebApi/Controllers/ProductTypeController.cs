﻿using Microsoft.AspNetCore.Mvc;
using productWebApi.Data;
using productWebApi.Models;
using Microsoft.AspNetCore.Http;
using System.Text;
using productWebApi.Models.searchModels;

using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using productWebApi.Models.helpModels;
using System.Collections;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Net.Http;

namespace productWebApi.Controllers;


[ApiController]
[Route("[controller]")]
public class ProductTypeController : ControllerBase
{
    private readonly ILogger<ProductTypeController> logger;
    private ProductNodeTypeDBContext productNodeTypeDBContext;

    public ProductTypeController(ProductNodeTypeDBContext _productNodeTypeDBContext, ILogger<ProductTypeController> _logger)
    {
        productNodeTypeDBContext = _productNodeTypeDBContext;
        logger = _logger;
    }

    [HttpPost]
    [Route("/ProductType/UploadHtmlFile")]
    public async Task<IActionResult> UploadHtml(List<IFormFile> files)
    {
        try
        {
            long size = files.Sum(f => f.Length);
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await formFile.CopyToAsync(memoryStream);
                        if (memoryStream.Length <= 2097152)
                        {
                            var file = new RealShipRaw()
                            {
                                Name = GetFileName(formFile.FileName),
                                FileContent = memoryStream.ToArray(),
                            };
                            var existFile = productNodeTypeDBContext.RealShipRaw.Where(x => x.Name == file.Name).FirstOrDefault();
                            if (existFile != null)
                            {
                                existFile.FileContent = file.FileContent;
                                existFile.URL = "";
                                existFile.Area = "";
                                //update
                                productNodeTypeDBContext.RealShipRaw.Update(existFile);
                            }
                            else
                            {
                                //insert
                                productNodeTypeDBContext.RealShipRaw.Add(file);
                            }
                            await productNodeTypeDBContext.SaveChangesAsync();
                        }
                        else
                        {
                            return new JsonResult("{'result':'-1','msg':'文件超过了2MB'}");
                        }
                    }
                }
            }
            return new JsonResult("{'result':1,'msg':'上传成功'}");
        }
        catch (Exception e)
        {
            logger.LogError("上传HTML文件失败，原因：" + e.Message);
            return new JsonResult("{'result':'-2','msg':'上传失败'}");
        }
    }


    public string GetFileName(string fileName)
    {
        string name = "";
        if (fileName.Length > 0 && fileName.Split('.').Count() > 0)
        {
            string[] arrayName = fileName.Split('.');
            name = string.Join('.', arrayName.Take(arrayName.Length - 1).ToArray());
        }
        return name.ToUpper();
    }

    [HttpGet]
    [Route("/ProductType/ShowHtmlFiles")]
    public IEnumerable<RealShipRawAll> ShowHtmlFiles()
    {
        var Data = productNodeTypeDBContext.NodeRaw.Where(w => w.Length == 0).GroupBy(a => a.RealShipRaw.ID)
            .Select(
            g => new
            {
                ID = g.Key,
                count = g.Count()
            }).Union(productNodeTypeDBContext.ProductRaw.Where(w => w.Length == 0).GroupBy(a => a.RealShipRaw.ID)
            .Select(
            g => new
            {
                ID = g.Key,
                count = g.Count()
            })).GroupBy(b => b.ID).Select(s => new
            {
                ID = s.Key,
                sumCount = s.Sum(i => i.count)
            });

        var d = from n in productNodeTypeDBContext.RealShipRaw
                join a in Data on n.ID equals a.ID
                into re
                from r in re.DefaultIfEmpty()
                select new RealShipRawAll
                {
                    ID = n.ID,
                    Name = n.Name,
                    URL = n.URL,
                    Area = n.Area,
                    Count = r.sumCount
                };
        return d.ToArray();
    }

    [HttpGet]
    [Route("/ProductType/ShowRealShipByIdN")]
    public pagingModel<NodeRaw> ShowRealShipByIdN(int id, int currentPage = 0, int pagesize = 10)
    {
        int totalCounts = productNodeTypeDBContext.NodeRaw.Where(x => x.RealShipRaw.ID == id).Count();
        var items = productNodeTypeDBContext.NodeRaw.Where(x => x.RealShipRaw.ID == id).OrderByDescending(o => o.Length)
            .Skip(pagesize * (currentPage - 1)).Take(pagesize).Select(s => s);
        pagingModel<NodeRaw> myPageModel = new pagingModel<NodeRaw>()
        {
            PageSize = pagesize,
            CurrentPage = currentPage,
            TotalCount = totalCounts,
            PageCount = totalCounts % pagesize > 0 ? totalCounts / pagesize + 1 : totalCounts / pagesize,
            Lists = items.AsQueryable()
        };
        return myPageModel;
    }
 


    [HttpGet]
    [Route("/ProductType/ShowRealShipByIdP")]
    public pagingModel<ProductRaw> ShowRealShipByIdP(int id, int currentPage = 0, int pagesize = 10)
    {
        int totalCounts = productNodeTypeDBContext.ProductRaw.Where(x => x.RealShipRaw.ID == id).Count();
        var items = productNodeTypeDBContext.ProductRaw.Where(x => x.RealShipRaw.ID == id).OrderByDescending(o => o.Length)
            .Skip(pagesize * (currentPage - 1)).Take(pagesize).Select(s => s);
        
        pagingModel <ProductRaw> myPageModel = new pagingModel<ProductRaw>()
        {
            PageSize = pagesize,
            CurrentPage = currentPage,
            TotalCount = totalCounts,
            PageCount= totalCounts % pagesize > 0 ? totalCounts / pagesize + 1 : totalCounts / pagesize,
        Lists = items.AsQueryable()
        };
        return myPageModel;
        //productNodeTypeDBContext.ProductRaw.Where(x => x.RealShipRaw.ID == id).OrderByDescending(o => o.Length)
        //.Select(s => new ProductRaw
        //{
        //    ID = s.ID,
        //    Length = s.Length,
        //    URL = s.URL,
        //    Name = s.Name
        //}).ToArray();
    }

    [HttpGet]
    [Route("/ProductType/ShowRealShipById")]
    public IEnumerable<RealShipRawDetail> ShowRealShipById(int id)
    {
        IEnumerable<RealShipRawDetail> realShipRawDetailsp = productNodeTypeDBContext.ProductRaw
            .Where(x => x.RealShipRaw.ID == id && x.Length == 0)
             .OrderByDescending(o => o.Length).Select(s => new RealShipRawDetail
             {
                 ID = s.ID,
                 RealShipRawID = id,
                 Type = "p",
                 URL = s.URL
             });

        IEnumerable<RealShipRawDetail> realShipRawDetailsn = productNodeTypeDBContext.NodeRaw
            .Where(x => x.RealShipRaw.ID == id && x.Length == 0)
            .OrderByDescending(o => o.Length).Select(s => new RealShipRawDetail
            {
                ID = s.ID,
                RealShipRawID = id,
                Type = "n",
                URL = s.URL
            });
        return realShipRawDetailsp.Union(realShipRawDetailsn);
    }

    [HttpGet]
    [Route("/ProductType/AnalysisHtml")]
    public IActionResult AnalysisHtml(int id)
    {
        var RealShipRaw = productNodeTypeDBContext.RealShipRaw.Where(x => x.ID == id).FirstOrDefault();
        if (RealShipRaw.IsNotNull())
        {
            Sy.String Page = Encoding.UTF8.GetString(RealShipRaw!.FileContent);
            const string AreaSpliter = "'Events.SushiEndpoint': '";
            if (Page.Contains(AreaSpliter))
            {
                RealShipRaw.Area = Page.SplitEx(AreaSpliter, 1).Mid("amazon.", "/");
            }
            else
            {
                return new JsonResult("{'result':'-1','msg':'解析内容出错[NotFoundAreaSpliter]'}");
            }
            var TableMidor = new Midor("<table", "</table>");
            if (Page.CanMid(TableMidor))
            {
                var TableContent = Page.Mid(TableMidor, Sy.StringMidOptions.StartFormBack_EndFormFront);
                var RowMidor = new Midor("<tr>", "</tr>");
                if (TableContent.CanMid(RowMidor))
                {
                    var Enumerator = TableContent.GetMidEnumerator(RowMidor);
                    List<Sy.String[]> LstRowUseful = new();
                    if (Enumerator.MoveNext())
                    {
                        //解析Thead->Tr->Th
                        const string ThEnd = "</th>";
                        const string TdEnd = "</td>";
                        var Count = Enumerator.Current.Split(ThEnd).Length - 1;  //判断存在几列数据
                        short BTGIndex = -1;//分类数表索引 - xls
                        short CCTIndex = -1;//分类模板索引 - xlsm
                        Dictionary<int, int> NeedSpan = new();
                        while (Enumerator.MoveNext())
                        {
                            if (Enumerator.Current.Contains("</a>"))
                            {
                                var Temp = Enumerator.Current.Split(TdEnd);
                                if (CCTIndex == -1)
                                {
                                    for (short i = 0; i < Temp.Length; i++)
                                    {
                                        if (Temp[i].Contains("xlsm"))
                                        {
                                            CCTIndex = i;
                                            break;
                                        }
                                    }
                                    if (CCTIndex != -1)
                                    {// xls m
                                        for (short i = 0; i < Temp.Length; i++)
                                        {
                                            if (CCTIndex != i && Temp[i].Contains("xls"))
                                            {
                                                BTGIndex = i;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (NeedSpan.Count > 0)
                                {
                                    var Lst = Temp.ToList();
                                    List<int> LstDel = new();
                                    foreach (var key in NeedSpan.Keys.OrderBy((i) => i))
                                    {
                                        var LastNum = NeedSpan[key];
                                        Lst.Insert(key, null);
                                        if (--LastNum == 0)
                                        {
                                            LstDel.Add(key);
                                        }
                                    }
                                    Temp = Temp.ToArray();
                                    LstDel.ForEach(i => NeedSpan.Remove(i));
                                }
                                LstRowUseful.Add(Temp);
                                for (short i = 0; i < Temp.Length; i++)
                                {
                                    var td = Temp[i];
                                    const string Span = "rowspan=\"";
                                    if (td.Contains(Span))
                                    {
                                        var numstr = td.Mid(Span);
                                        if (int.TryParse(numstr, out var num))
                                        {
                                            NeedSpan.Add(i, --num);
                                        }
                                        else
                                        {
                                            return new JsonResult("{'result':'-1','msg':'解析内容出错[InvalidCast]'}");
                                        }
                                    }
                                }
                            }
                        };
                        if (LstRowUseful.Count > 0 && BTGIndex != -1 && CCTIndex != -1)
                        {
                            List<RealShip> RealShips = new();
                            List<NodeRaw> NodeRaws = new();
                            NodeRaw LastNodeRaw = null;
                            List<ProductRaw> ProductRaws = new();
                            ProductRaw LastProductRaw = null;
                            foreach (var Cols in LstRowUseful)
                            {
                                if (Cols.Length <= Math.Max(BTGIndex, CCTIndex))
                                {
                                    return new JsonResult("{'result':'-1','msg':'解析内容出错[IndexOutOfRange]'}");
                                }
                                else
                                {
                                    const string Href = "href=\"";
                                    var BTG = Cols[BTGIndex];
                                    if (BTG.IsNotNull())
                                    {
                                        if (BTG.Contains(Href))
                                        {
                                            NodeRaws.Add(LastNodeRaw = new NodeRaw(RealShipRaw, BTG.Mid(Href)));
                                        }
                                        else
                                        {
                                            LastNodeRaw = null;
                                        }
                                    }
                                    var CCT = Cols[CCTIndex];
                                    if (CCT.IsNotNull())
                                    {
                                        if (CCT.Contains(Href))
                                        {
                                            ProductRaws.Add(LastProductRaw = new ProductRaw(RealShipRaw, CCT.Mid(Href)));
                                        }
                                        else
                                        {
                                            LastProductRaw = null;
                                        }
                                    }
                                    if (LastProductRaw.IsNull() && LastNodeRaw.IsNull())
                                    {
                                        return new JsonResult("{'result':'-1','msg':'解析内容出错[NullReference]'}");
                                    }
                                    RealShips.Add(new(RealShipRaw, LastNodeRaw!, LastProductRaw!));
                                }
                            }
                            productNodeTypeDBContext.AddRange(RealShips.ToArray());
                            productNodeTypeDBContext.SaveChanges();
                            return new JsonResult("{'result':'1','msg':'解析完成'}");
                        }
                        else
                        {
                            return new JsonResult("{'result':'-1','msg':'error'}");
                        }
                    }
                    else
                    {
                        return new JsonResult("{'result':'-1','msg':'解析内容出错[NotFoundThead]'}");
                    }
                }
                else
                {
                    return new JsonResult("{'result':'-1','msg':'解析内容出错[NotFoundTr]'}");
                }
            }
            else
            {
                return new JsonResult("{'result':'-1','msg':'解析内容出错[NotFoundTable]'}");
            }
        }
        else
        {
            // { "name":"Matilda"}
            return new JsonResult("{'result':'-1','msg':'记录不存在'}");
        }
    }

    [HttpGet]
    [Route("/ProductType/DownloadExcel")]
    public IActionResult DownloadExcel(int id, string type)
    {
        HttpClient client = new HttpClient();
        if (type == "n")
        {
            var NodeRaw = productNodeTypeDBContext.NodeRaw.Where(x => x.ID == id).FirstOrDefault();
            if (NodeRaw.IsNotNull())
            {
                try
                {
                    var files = client.GetByteArrayAsync(NodeRaw.URL);
                    NodeRaw.FileContent = files.Result;
                    NodeRaw.Length = files.Result.Length;
                    NodeRaw.AnsiTime = DateTime.Now;
                    productNodeTypeDBContext.NodeRaw.Update(NodeRaw);
                    productNodeTypeDBContext.SaveChanges();
                }
                catch (Exception e)
                {
                    logger.LogError("下载excel入库出错，ex：" + e.Message);
                    return new JsonResult("{'result':'-3','msg':'[DownloadError]'}");
                }
            }
            else
            {
                //node id wrong
                return new JsonResult("{'result':'-2','msg':'[NodeIDError]'}");
            }
        }
        else if (type == "p")
        {
            var ProductRaw = productNodeTypeDBContext.ProductRaw.Where(x => x.ID == id).FirstOrDefault();

            if (ProductRaw.IsNotNull())
            {
                try
                {
                    var files = client.GetByteArrayAsync(ProductRaw.URL);
                    ProductRaw.FileContent = files.Result;
                    ProductRaw.Length = files.Result.Length;
                    ProductRaw.AnsiTime = DateTime.Now;
                    productNodeTypeDBContext.ProductRaw.Update(ProductRaw);
                    productNodeTypeDBContext.SaveChanges();
                }
                catch (Exception e)
                {
                    logger.LogError("下载excel入库出错，ex：" + e.Message);
                    return new JsonResult("{'result':'-3','msg':'[DownloadError]'}");
                }
            }
            else
            {
                //product id wrong
                return new JsonResult("{'result':'-2','msg':'[ProductIDError]'}");
            }
        }
        else
        {
            //params wrong
            return new JsonResult("{'result':'-2','msg':'参数传递错误'}");
        }
        return new JsonResult("{'result':'1','msg':'下载成功'}");
    }


    [HttpGet]
    [Route("/ProductType/ClearData")]
    public IActionResult ClearData(int id)
    {
        //清空操作

        //RealShip
        var RealShip = productNodeTypeDBContext.RealShip.Where(x => x.RealShipRaw.ID == id).ToList();
        if (RealShip.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(RealShip);
        }

        //Node
        var Node = productNodeTypeDBContext.Node.Where(x => x.RSRID==id).ToList();
        if (Node.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(Node);
        }

        //NodeRaw
        var NodeRaw = productNodeTypeDBContext.NodeRaw.Where(x => x.RealShipRaw.ID == id).ToList();
        if (NodeRaw.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(NodeRaw);
        }

        //ProductType
        var ProductType = productNodeTypeDBContext.ProductType.Where(x => x.RSRID == id).ToList();
        if (ProductType.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(ProductType);
        }

        //DataDefinitions
        var DataDefinitions = productNodeTypeDBContext.DataDefinitions.Where(x => x.RSRID == id).ToList();
        if (DataDefinitions.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(DataDefinitions);
        }

        //ValidesValues
        var ValidesValues = productNodeTypeDBContext.ValidesValues.Where(x => x.RSRID == id).ToList();
        if (ValidesValues.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(ValidesValues);
        }

        //ProductRaw
        var ProductRaw = productNodeTypeDBContext.ProductRaw.Where(x => x.RealShipRaw.ID == id).ToList();
        if (ProductRaw.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(ProductRaw);
        }

        //RealShipRaw
        var RealShipRaw = productNodeTypeDBContext.RealShipRaw.Where(x => x.ID == id).ToList();
        if (RealShipRaw.IsNotNull())
        {
            productNodeTypeDBContext.RemoveRange(RealShipRaw);
        }
        productNodeTypeDBContext.SaveChanges();
        return new JsonResult("{'result':'1','msg':'删除成功'}");
    }

    [HttpGet]
    [Route("/ProductType/GetNodeExcelList")]
    public IEnumerable<NodeRaw> GetNodeExcelList(int id) =>
     productNodeTypeDBContext.NodeRaw.Where(x => x.RealShipRaw.ID == id).ToList();


    [HttpGet]
    [Route("/ProductType/analysisNExcelClick")]
    public IActionResult analysisNExcelClick(int id, int rID)
    {
        try
        {
            //获取execl内容，解析
            var n = productNodeTypeDBContext.NodeRaw.Where(x => x.ID == id).FirstOrDefault();
            if (n.IsNotNull())
            {
                List<Node> nodes = new List<Node>();
                Dictionary<int, keyVal> keyValues = new Dictionary<int, keyVal>();
                for (int i = 0; i < 10; i++)
                {
                    keyValues.Add(i, new keyVal());
                }
                int rSRID = rID;
                int formRawID = n.ID;
                string fileName = n.Name;
                using (MemoryStream memStream = new MemoryStream(n.FileContent))
                {
                    IWorkbook workbook = null;
                    string _ext = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf(".")).ToLower();
                    if (_ext == ".xlsx" || _ext == ".xlsm")
                    {
                        workbook = new XSSFWorkbook(memStream);
                    }
                    else
                    {
                        workbook = new HSSFWorkbook(memStream);
                    }

                    ISheet sheet = workbook.GetSheetAt(1);
                    IRow ITitleRow = sheet.GetRow(0);  //第一行，列名
                    int totalColumn = ITitleRow.LastCellNum;
                    int totalRow = sheet.LastRowNum;
                    string _value = string.Empty;
                    string _type = string.Empty;
                    for (int i = 1; i <= totalRow; i++)
                    {
                        string nameNext = "";
                        IRow row = sheet.GetRow(i);
                        IRow rowNext = sheet.GetRow(i + 1);
                        if (i != totalRow)  //最后一行就不赋值了，直接置为空
                        {
                            nameNext = rowNext.GetCell(1).ToString();
                        }

                        if (row == null)
                        {
                            continue;
                        }
                        string forName = row.GetCell(1).ToString();
                        string name = "";
                        if (!forName.Contains("/"))
                        {
                            name = forName;
                        }
                        else
                        {
                            name = forName.Substring(forName.LastIndexOf("/") + 1, forName.Length - forName.LastIndexOf("/") - 1);
                        }
                        string value = row.GetCell(0).ToString();
                        int level = forName.Split('/').Length - 1;
                        if (level == 0)
                        {
                            //parentid  null，直接入库
                            keyValues[0].Name = forName;
                            Node n0 = new Node(null, formRawID, rSRID, forName, name, value);
                            productNodeTypeDBContext.Node.Add(n0);
                            productNodeTypeDBContext.SaveChanges();
                            keyValues[0].ID = n0.ID;
                        }
                        else
                        {
                            keyValues[level].Name = forName;
                            Node n1 = new Node(keyValues[level - 1].ID, formRawID, rSRID, forName, name, value);
                            if (!nameNext.Contains(forName))
                            {
                                //是终结点，存入list中
                                nodes.Add(n1);

                            }
                            else
                            {
                                //非终结点，save，更新id              
                                productNodeTypeDBContext.Node.Add(n1);
                                productNodeTypeDBContext.SaveChanges();
                                keyValues[level].ID = n1.ID;
                            }
                        }
                    }
                    productNodeTypeDBContext.Node.AddRange(nodes);
                    productNodeTypeDBContext.SaveChanges();
                }
            }
            return new JsonResult("{'result':'1','msg':'excel解析成功'}");
        }
        catch (Exception e)
        {
            logger.LogError("node:" + id.ToString() + "的excel解析失败，" + e.Message);
            return new JsonResult("{'result':'-1','msg':'excel解析失败'}");
        }
    }

    [HttpGet]
    [Route("/ProductType/GetProductExcelList")]
    public IEnumerable<ProductRaw> GetProductExcelList(int id) =>
     productNodeTypeDBContext.ProductRaw.Where(x => x.RealShipRaw.ID == id).ToList();

    [HttpGet]
    [Route("/ProductType/analysisPExcelClick")]
    public IActionResult analysisPExcelClick(int id, int rID)
    {
        try
        {
            //获取execl内容，解析
            var n = productNodeTypeDBContext.ProductRaw.Where(x => x.ID == id).FirstOrDefault();
            if (n.IsNotNull())
            {

                int rSRID = rID;
                int formRawID = n.ID;
                string fileName = n.Name;
                using (MemoryStream memStream = new MemoryStream(n.FileContent))
                {
                    IWorkbook workbook = null;
                    string _ext = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf(".")).ToLower();
                    if (_ext == ".xlsx" || _ext == ".xlsm")
                    {
                        workbook = new XSSFWorkbook(memStream);
                    }
                    else
                    {
                        workbook = new HSSFWorkbook(memStream);
                    }
                    //analysis Data Definitions
                    ISheet sheetAttribute = workbook.GetSheet("Data Definitions");
                    AnalysisAttributes(sheetAttribute, id, rID);
                    //analysis productType
                    ISheet sheetProductType = workbook.GetSheet("Valid Values");
                    AnalysisProductType(sheetProductType, id, rID);
                }
            }
            return new JsonResult("{'result':'1','msg':'excel解析成功'}");
        }
        catch (Exception e)
        {
            logger.LogError("node:" + id.ToString() + "的excel解析失败，" + e.Message);
            return new JsonResult("{'result':'-1','msg':'excel解析失败'}");
        }
    }

    public int AnalysisAttributes(ISheet sheet, int formRawID, int rID)
    {
        try
        {
            List<DataDefinitions> definitions = new List<DataDefinitions>();
            IRow ITitleRow = sheet.GetRow(1);        //第二行，列名
            int totalColumn = ITitleRow.LastCellNum;
            int totalRow = sheet.LastRowNum;
            for (int i = 2; i <= totalRow; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null)
                {
                    continue;
                }
             
                if(row.GetCell(0).IsNotNull() && row.GetCell(0).ToString().IsNotWhiteSpace())
                {
                    string GroupName = row.GetCell(0).ToString();
                    continue;
                }
                //取值
                string name = row.GetCell(2).ToString().Trim();
                string feedName = row.GetCell(1).ToString().Trim();
                string definitionAndUse = row.GetCell(3).ToString().Trim();
                string acceptedValues = row.GetCell(4).ToString().Trim();
                string example = row.GetCell(5).ToString().Trim();
                string Required = row.GetCell(6).ToString().Trim().ToUpper();
                var type = RequireType.Require;
                ////怎么写优雅一点
                if ("REQUIRED".Equals(Required))
                {
                    type=RequireType.Require;
                }
                else if ("OPTIONAL".Equals(Required))
                {
                    type=RequireType.Optional;
                }
                else if ("DESIRED".Equals(Required))
                {
                    type=RequireType.Desired;                       
                }

                DataDefinitions data = new DataDefinitions(formRawID, rID, name, feedName, type,
                    definitionAndUse, acceptedValues, example);
                definitions.Add(data);
            }
            productNodeTypeDBContext.DataDefinitions.AddRange(definitions);
            productNodeTypeDBContext.SaveChanges();
            return 1;
        }
        catch (Exception e)
        {
            logger.LogError("解析产品属性失败，原因：" + e.Message);
            return -1;
        }
    }

    public int AnalysisProductType(ISheet sheet, int formRawID, int rID)
    {
        try
        {
            List<ValidesValues> validesValues = new List<ValidesValues>();
            IRow ITitleRow = sheet.GetRow(1);        //第二行，列名
            int totalColumn = ITitleRow.LastCellNum;
            int totalRow = sheet.LastRowNum;
            for (int i = 1; i <= totalRow; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null || row.GetCell(0).IsNotNull())  //没有值或者是分组行，直接跳过
                {
                    continue;
                }
                int totalCell = row.LastCellNum;  //每一行的单元格总数
                //第一行，取productType
                if (i == 1)
                {
                    List<ProductType> productTypes = new List<ProductType>();                   
                    for(int j=2;j< row.LastCellNum;j++)
                    {
                        string name = row.GetCell(j).ToString();
                        if (name.IsNotWhiteSpace())
                        {
                            ProductType product = new ProductType(formRawID, rID, name);
                            productTypes.Add(product);
                        }
                    }                                     
                    productNodeTypeDBContext.ProductType.AddRange(productTypes);
                    productNodeTypeDBContext.SaveChanges();
                }
                else
                {
                    //获取产品类别和产品属性，放入内存
                    List<ProductType> proTypes = productNodeTypeDBContext.ProductType.Where(x => x.FromRawID == formRawID).ToList();
                    List<DataDefinitions> dataDefinitions = productNodeTypeDBContext.DataDefinitions.Where(x => x.FromRawID == formRawID).ToList();

                    int PTID = 0, DFID = 0;
                    //处理产品属性和产品类别的关系
                    string strD = row.GetCell(1).ToString().Trim();
                    if (strD.Contains('-'))
                    {
                        string[] arrStr = strD.Split('-');
                        string strT = arrStr[0].ToString().Trim();
                        var p = proTypes.Where(x => x.Name == strT).FirstOrDefault();
                        if (p.IsNotNull())
                        {
                            PTID = p.ID;
                        }
                        strD = arrStr[0].ToString().Trim();
                    }

                    var d = dataDefinitions.Where(x => x.Name == strD).FirstOrDefault();
                    if (d.IsNotNull())
                    {
                        DFID = d.ID;
                        d.Type = FieldType.SelectOrOptional;
                        productNodeTypeDBContext.DataDefinitions.Update(d);
                    }
                    else
                    {
                        logger.LogError($"fromRawID:{0},{1}没有找到他的属性ID", formRawID, strD);
                    }
                    List<string> datas = new List<string>();
                    for (int j = 2; j < totalCell; j++)
                    {
                        //获取值类型范围，拼接json
                        string value = row.GetCell(j).ToString();
                        if (value.IsNotWhiteSpace())
                        {
                            datas.Add(value.Trim());
                        }
                    }
                    //入库ValidesValues
                    string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(datas);

                    ValidesValues valides = new ValidesValues(DFID, PTID, formRawID, rID, jsons);
                    validesValues.Add(valides);
                }
            }
            productNodeTypeDBContext.ValidesValues.AddRange(validesValues);
            productNodeTypeDBContext.SaveChanges();
            return 1;
        }
        catch (Exception e)
        {
            logger.LogError("解析产品Excel失败，原因：" + e.Message);
            return -1;
        }
    }


    [HttpGet]
    [Route("/ProductType/Test")]
    public void Test()
    {
        //array的声明
        string[] arrayStr = new string[7];
        string[] arrayStr2 = new string[] { "2", "3", "4" };

        int[] myIntArray = new int[5] { 1, 2, 3, 4, 5 };
        Object[] myObjArray = new Object[5] { 26, 27, 28, 29, 30 };

        //从int中拷贝前两个到obj中
        System.Array.Copy(myIntArray, myObjArray, 2);
        //

        string[,] arrayStr3 = new string[,] {{ "2", "2" }, { "3", "3" }, { "4", "4" } };
        int n= arrayStr3.Length;
       
        ArrayList arrayList = new ArrayList();
        

        //KeyValuePair<TKey, TValue> 结构类型
        Dictionary<int, string> keyValues = new Dictionary<int, string>();
        //


        List<string> lists = new List<string>();



        Hashtable hashtable = new Hashtable();
        foreach(DictionaryEntry de in hashtable)
        {

        }
    }
}