﻿
using Microsoft.EntityFrameworkCore;
using productWebApi.Models;

namespace productWebApi.Data;
public class ProductNodeTypeDBContext: DbContext
{
    public DbSet<RealShipRaw> RealShipRaw { get; set; }
    public DbSet<RealShip> RealShip { get; set; }
    public DbSet<NodeRaw> NodeRaw { get; set; }
    public DbSet<ProductRaw> ProductRaw { get; set; }

    public DbSet<Node> Node { get; set; }

    public DbSet<ProductType> ProductType { get; set; }

    public DbSet<DataDefinitions> DataDefinitions { get; set; }

    public DbSet<ValidesValues> ValidesValues { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=productType.dmp");
    }
}
