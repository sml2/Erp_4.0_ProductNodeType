﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace productWebApi.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RealShipRaw",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Area = table.Column<string>(type: "TEXT", nullable: true),
                    URL = table.Column<string>(type: "TEXT", nullable: true),
                    FileContent = table.Column<byte[]>(type: "BLOB", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RealShipRaw", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "NodeRaw",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RealShipRawID = table.Column<int>(type: "INTEGER", nullable: true),
                    Length = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    URL = table.Column<string>(type: "TEXT", nullable: true),
                    FileContent = table.Column<byte[]>(type: "BLOB", nullable: true),
                    GetTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    AnsiTime = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NodeRaw", x => x.ID);
                    table.ForeignKey(
                        name: "FK_NodeRaw_RealShipRaw_RealShipRawID",
                        column: x => x.RealShipRawID,
                        principalTable: "RealShipRaw",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "ProductRaw",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RealShipRawID = table.Column<int>(type: "INTEGER", nullable: true),
                    Length = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    URL = table.Column<string>(type: "TEXT", nullable: true),
                    FileContent = table.Column<byte[]>(type: "BLOB", nullable: true),
                    GetTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    AnsiTime = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRaw", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductRaw_RealShipRaw_RealShipRawID",
                        column: x => x.RealShipRawID,
                        principalTable: "RealShipRaw",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "RealShip",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RealShipRawID = table.Column<int>(type: "INTEGER", nullable: true),
                    NodeRawID = table.Column<int>(type: "INTEGER", nullable: true),
                    ProductRawID = table.Column<int>(type: "INTEGER", nullable: true),
                    Area = table.Column<string>(type: "TEXT", nullable: false),
                    Type = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RealShip", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RealShip_NodeRaw_NodeRawID",
                        column: x => x.NodeRawID,
                        principalTable: "NodeRaw",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_RealShip_ProductRaw_ProductRawID",
                        column: x => x.ProductRawID,
                        principalTable: "ProductRaw",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_RealShip_RealShipRaw_RealShipRawID",
                        column: x => x.RealShipRawID,
                        principalTable: "RealShipRaw",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_NodeRaw_RealShipRawID",
                table: "NodeRaw",
                column: "RealShipRawID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRaw_RealShipRawID",
                table: "ProductRaw",
                column: "RealShipRawID");

            migrationBuilder.CreateIndex(
                name: "IX_RealShip_NodeRawID",
                table: "RealShip",
                column: "NodeRawID");

            migrationBuilder.CreateIndex(
                name: "IX_RealShip_ProductRawID",
                table: "RealShip",
                column: "ProductRawID");

            migrationBuilder.CreateIndex(
                name: "IX_RealShip_RealShipRawID",
                table: "RealShip",
                column: "RealShipRawID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RealShip");

            migrationBuilder.DropTable(
                name: "NodeRaw");

            migrationBuilder.DropTable(
                name: "ProductRaw");

            migrationBuilder.DropTable(
                name: "RealShipRaw");
        }
    }
}
