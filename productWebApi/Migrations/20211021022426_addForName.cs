﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace productWebApi.Migrations
{
    public partial class addForName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ForName",
                table: "Node",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ForName",
                table: "Node");
        }
    }
}
