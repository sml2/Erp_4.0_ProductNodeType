﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace productWebApi.Migrations
{
    public partial class updateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FeedName",
                table: "DataDefinitions");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                comment: "展示类型：文本，下拉框",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "Rule",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                comment: "必填，选填，建议",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                comment: "HtmlID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "PTID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: true,
                comment: "ProductTypeID",
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FromRawID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                comment: "ProductRawID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Data",
                table: "ValidesValues",
                type: "TEXT",
                nullable: false,
                comment: "values json",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "DFID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                comment: "DataDefinitionsID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "URL",
                table: "RealShipRaw",
                type: "TEXT",
                nullable: true,
                comment: "Html文件的url地址",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "RealShipRaw",
                type: "TEXT",
                nullable: false,
                comment: "Html文件的名称",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileContent",
                table: "RealShipRaw",
                type: "BLOB",
                nullable: true,
                comment: "Html文件内容",
                oldClrType: typeof(byte[]),
                oldType: "BLOB",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Area",
                table: "RealShipRaw",
                type: "TEXT",
                nullable: true,
                comment: "所属地区/国家",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "RealShipRaw",
                type: "INTEGER",
                nullable: false,
                comment: "HtmlID",
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .Annotation("Sqlite:Autoincrement", true)
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "RealShip",
                type: "INTEGER",
                nullable: false,
                comment: "区分是直接关系，还是间接引用的其他的",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Area",
                table: "RealShip",
                type: "TEXT",
                nullable: false,
                comment: "所属地区、国家",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "ProductType",
                type: "INTEGER",
                nullable: false,
                comment: "HtmlID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductType",
                type: "TEXT",
                nullable: false,
                comment: "产品类别",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "FromRawID",
                table: "ProductType",
                type: "INTEGER",
                nullable: false,
                comment: "ProductRawID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "URL",
                table: "ProductRaw",
                type: "TEXT",
                nullable: true,
                comment: "ProductExcel的Url",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductRaw",
                type: "TEXT",
                nullable: false,
                comment: "ProductExcel的名称",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "Length",
                table: "ProductRaw",
                type: "INTEGER",
                nullable: false,
                comment: "ProductExcel的长度",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<DateTime>(
                name: "GetTime",
                table: "ProductRaw",
                type: "TEXT",
                nullable: true,
                comment: "ProductExcel的获取时间",
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileContent",
                table: "ProductRaw",
                type: "BLOB",
                nullable: true,
                comment: "ProductExcel的内容",
                oldClrType: typeof(byte[]),
                oldType: "BLOB",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AnsiTime",
                table: "ProductRaw",
                type: "TEXT",
                nullable: true,
                comment: "ProductExcel的解析时间",
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "URL",
                table: "NodeRaw",
                type: "TEXT",
                nullable: true,
                comment: "NodeExcel的Url",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "NodeRaw",
                type: "TEXT",
                nullable: false,
                comment: "NodeExcel名称",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "Length",
                table: "NodeRaw",
                type: "INTEGER",
                nullable: false,
                comment: "NodeExcel内容长度",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<DateTime>(
                name: "GetTime",
                table: "NodeRaw",
                type: "TEXT",
                nullable: false,
                comment: "NodeExcel的获取日期",
                oldClrType: typeof(DateTime),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileContent",
                table: "NodeRaw",
                type: "BLOB",
                nullable: true,
                comment: "NodeExcel的解析内容",
                oldClrType: typeof(byte[]),
                oldType: "BLOB",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AnsiTime",
                table: "NodeRaw",
                type: "TEXT",
                nullable: true,
                comment: "NodeExcel的解析日期",
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "Node",
                type: "TEXT",
                nullable: false,
                comment: "Node编号",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "Node",
                type: "INTEGER",
                nullable: false,
                comment: "HtmlID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "ParentID",
                table: "Node",
                type: "INTEGER",
                nullable: true,
                comment: "父NodeID",
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Node",
                type: "TEXT",
                nullable: false,
                comment: "当前name节点",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "FormRawID",
                table: "Node",
                type: "INTEGER",
                nullable: false,
                comment: "NodeExcelID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "ForName",
                table: "Node",
                type: "TEXT",
                nullable: false,
                comment: "所有name节点",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                comment: "展示类型：文本，下拉框",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "Rule",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                comment: "必填，选填，建议",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                comment: "HtmlID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: false,
                comment: "产品属性",
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "FromRawID",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                comment: "ProductRawID",
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Example",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                comment: "例子",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DefinitionAndUse",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                comment: "定义和使用",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Data",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                comment: "值-并集",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AcceptedValues",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                comment: "接受的值描述",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FieldName",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: false,
                defaultValue: "",
                comment: "数据库使用的产品属性");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FieldName",
                table: "DataDefinitions");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "展示类型：文本，下拉框");

            migrationBuilder.AlterColumn<int>(
                name: "Rule",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "必填，选填，建议");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "HtmlID");

            migrationBuilder.AlterColumn<int>(
                name: "PTID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true,
                oldComment: "ProductTypeID");

            migrationBuilder.AlterColumn<int>(
                name: "FromRawID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "ProductRawID");

            migrationBuilder.AlterColumn<string>(
                name: "Data",
                table: "ValidesValues",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "values json");

            migrationBuilder.AlterColumn<int>(
                name: "DFID",
                table: "ValidesValues",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "DataDefinitionsID");

            migrationBuilder.AlterColumn<string>(
                name: "URL",
                table: "RealShipRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "Html文件的url地址");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "RealShipRaw",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "Html文件的名称");

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileContent",
                table: "RealShipRaw",
                type: "BLOB",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "BLOB",
                oldNullable: true,
                oldComment: "Html文件内容");

            migrationBuilder.AlterColumn<string>(
                name: "Area",
                table: "RealShipRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "所属地区/国家");

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "RealShipRaw",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "HtmlID")
                .Annotation("Sqlite:Autoincrement", true)
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "RealShip",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "区分是直接关系，还是间接引用的其他的");

            migrationBuilder.AlterColumn<string>(
                name: "Area",
                table: "RealShip",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "所属地区、国家");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "ProductType",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "HtmlID");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductType",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "产品类别");

            migrationBuilder.AlterColumn<int>(
                name: "FromRawID",
                table: "ProductType",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "ProductRawID");

            migrationBuilder.AlterColumn<string>(
                name: "URL",
                table: "ProductRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "ProductExcel的Url");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductRaw",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "ProductExcel的名称");

            migrationBuilder.AlterColumn<int>(
                name: "Length",
                table: "ProductRaw",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "ProductExcel的长度");

            migrationBuilder.AlterColumn<DateTime>(
                name: "GetTime",
                table: "ProductRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "ProductExcel的获取时间");

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileContent",
                table: "ProductRaw",
                type: "BLOB",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "BLOB",
                oldNullable: true,
                oldComment: "ProductExcel的内容");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AnsiTime",
                table: "ProductRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "ProductExcel的解析时间");

            migrationBuilder.AlterColumn<string>(
                name: "URL",
                table: "NodeRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "NodeExcel的Url");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "NodeRaw",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "NodeExcel名称");

            migrationBuilder.AlterColumn<int>(
                name: "Length",
                table: "NodeRaw",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "NodeExcel内容长度");

            migrationBuilder.AlterColumn<DateTime>(
                name: "GetTime",
                table: "NodeRaw",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldComment: "NodeExcel的获取日期");

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileContent",
                table: "NodeRaw",
                type: "BLOB",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "BLOB",
                oldNullable: true,
                oldComment: "NodeExcel的解析内容");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AnsiTime",
                table: "NodeRaw",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "NodeExcel的解析日期");

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "Node",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "Node编号");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "Node",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "HtmlID");

            migrationBuilder.AlterColumn<int>(
                name: "ParentID",
                table: "Node",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true,
                oldComment: "父NodeID");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Node",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "当前name节点");

            migrationBuilder.AlterColumn<int>(
                name: "FormRawID",
                table: "Node",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "NodeExcelID");

            migrationBuilder.AlterColumn<string>(
                name: "ForName",
                table: "Node",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "所有name节点");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "展示类型：文本，下拉框");

            migrationBuilder.AlterColumn<int>(
                name: "Rule",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "必填，选填，建议");

            migrationBuilder.AlterColumn<int>(
                name: "RSRID",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "HtmlID");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldComment: "产品属性");

            migrationBuilder.AlterColumn<int>(
                name: "FromRawID",
                table: "DataDefinitions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldComment: "ProductRawID");

            migrationBuilder.AlterColumn<string>(
                name: "Example",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "例子");

            migrationBuilder.AlterColumn<string>(
                name: "DefinitionAndUse",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "定义和使用");

            migrationBuilder.AlterColumn<string>(
                name: "Data",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "值-并集");

            migrationBuilder.AlterColumn<string>(
                name: "AcceptedValues",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true,
                oldComment: "接受的值描述");

            migrationBuilder.AddColumn<string>(
                name: "FeedName",
                table: "DataDefinitions",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }
    }
}
