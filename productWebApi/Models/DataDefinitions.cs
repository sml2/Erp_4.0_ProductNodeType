﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
namespace productWebApi.Models;


public class DataDefinitions
{
    public DataDefinitions()
    {

    }
    public DataDefinitions(int fromRawID,int rSRID, string name,string fiedName, RequireType require,
        string definitionAndUse,string acceptedValues,string example)
    {
        FromRawID = fromRawID;
        RSRID = rSRID;
        Name = name;
        FieldName = fiedName;
        Rule = require;
        DefinitionAndUse = definitionAndUse;
        AcceptedValues = acceptedValues;
        Example = example;
    }
    public int ID { get; set; }

    [Comment("ProductRawID")]
    public int FromRawID { get; set; }

    [Comment("HtmlID")]
    public int RSRID { get; set; }

    [Comment("产品属性")]
    public string Name { get; set; }

    [Comment("数据库使用的产品属性")]
    public string FieldName { get; set; }

    [Comment("展示类型：文本，下拉框")]
    [DefaultValue(FieldType.TextORString)]   //SelectOrOptional
    public FieldType Type { get; set; }

    [Comment("必填，选填，建议")]
    public RequireType Rule { get; set; }

    [Comment("值-并集")]
    public string? Data { get; set; }

    [Comment("定义和使用")]
    public string? DefinitionAndUse { get; set; }

    [Comment("接受的值描述")]
    public string? AcceptedValues { get; set; }

    [Comment("例子")]
    public string? Example { get; set; }
}


public enum RequireType
{
    Require,
    Optional,
    Desired
}

public enum FieldType
{
    TextORString,
    SelectOrOptional
}
