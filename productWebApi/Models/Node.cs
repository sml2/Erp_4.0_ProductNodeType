﻿
using Microsoft.EntityFrameworkCore;

namespace productWebApi.Models;

public class Node
{
    public Node(int? parentID, int formRawID, int rSRID,string forName, string name, string value)
    {
        ParentID = parentID;
        FormRawID = formRawID;
        RSRID = rSRID;
        Name = name;
        Value = value;
        ForName = forName;
    }

    public int ID { get; set; }   

    [Comment("父NodeID")]
    public int? ParentID { set; get; }

    [Comment("NodeExcelID")]
    public int FormRawID { get; set; }

    [Comment("HtmlID")]
    public int RSRID { get; set; }  

    [Comment("所有name节点")]
    public string ForName { get; set; }

    [Comment("当前name节点")]
    public string Name { get; set; }

    [Comment("Node编号")]
    public string Value { get; set; }
}

