﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace productWebApi.Models;
/// <summary>
/// BTG
/// </summary>
public class NodeRaw
{
    public NodeRaw() { }
    public NodeRaw(RealShipRaw realShipRaw, string url)
    {
        RealShipRaw = realShipRaw;
        Name = Path.GetFileName(url);
        URL = url;
        GetTime = DateTime.Now;
    }
    public int ID { get; set; }

    [Comment("RealShipRawID")]
    public RealShipRaw RealShipRaw { get; set; }

    [Comment("NodeExcel内容长度")]
    public int Length { get; set; }

    [Comment("NodeExcel名称")]
    public string Name { get; set; }

    [Comment("NodeExcel的Url")]
    public string? URL { get; set; }

    [Comment("NodeExcel的解析内容")]
    public byte[]? FileContent { get; set; }

    [Comment("NodeExcel的获取日期")]
    public DateTime GetTime { get; set; }

    [Comment("NodeExcel的解析日期")]
    public DateTime? AnsiTime { get; set; }
}
