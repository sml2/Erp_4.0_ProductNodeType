﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace productWebApi.Models;
/// <summary>
/// CCT
/// </summary>
public class ProductRaw
{
    public ProductRaw() { }
    public ProductRaw(RealShipRaw realShipRaw)
    {
        RealShipRaw = realShipRaw;
    }
    public ProductRaw(RealShipRaw realShipRaw,string url):this(realShipRaw)
    {
        URL = url;
        Name = Path.GetFileName(url);
        GetTime = DateTime.Now;
    }
    public int ID { get; set; }

    [Comment("RealShipRawID")]
    public RealShipRaw RealShipRaw { get; set; }

    [Comment("ProductExcel的长度")]
    public int Length { get; set; }

    [Comment("ProductExcel的名称")]
    public string Name { get; set; }

    [Comment("ProductExcel的Url")]
    public string? URL { get; set; }

    [Comment("ProductExcel的内容")]
    public byte[]? FileContent { get; set; }

    [Comment("ProductExcel的获取时间")]
    public DateTime? GetTime { get; set; }

    [Comment("ProductExcel的解析时间")]
    public DateTime? AnsiTime { get; set; }
}