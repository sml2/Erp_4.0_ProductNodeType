﻿using Microsoft.EntityFrameworkCore;

namespace productWebApi.Models;

public class ProductType
{
    public ProductType(int fromRawID, int rSRID, string name)
    {
        FromRawID = fromRawID;
        RSRID = rSRID;
        Name = name;
    }
    public int ID { get; set; }
    [Comment("ProductRawID")]
    public int FromRawID { get; set; }
    [Comment("HtmlID")]
    public int RSRID { get; set; }
    [Comment("产品类别")]
    public string Name { get; set; }
}

