﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace productWebApi.Models;

public class RealShip
{
    public RealShip() { }
    public RealShip(RealShipRaw realShipRaw, NodeRaw nodeRaw, ProductRaw productRaw)
    {
        RealShipRaw = realShipRaw;
        Area = realShipRaw.Area!;
        NodeRaw = nodeRaw;
        ProductRaw = productRaw;
    }
 
    public int ID { get; set; }

    [Comment("RealShipRawID")]
    public RealShipRaw RealShipRaw { get; set; }

    [Comment("NodeRawID")]
    public NodeRaw? NodeRaw { get; set; }

    [Comment("ProductRawID")]
    public ProductRaw? ProductRaw { get; set; }

    [Comment("所属地区、国家")]
    public string Area { get; set; }

    [Comment("区分是直接关系，还是间接引用的其他的")]   
    public Types Type { get; set; }
    public enum Types
    {
        Dirct,Reference
    }
}
