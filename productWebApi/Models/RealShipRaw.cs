using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace productWebApi.Models;
public class RealShipRaw
{
    [Comment("HtmlID")]
    public int ID { get; set; }

    [Comment("Html文件的名称")]
    public string Name { get; set; }

    [Comment("所属地区/国家")]
    public string? Area { get; set; }

    [Comment("Html文件的url地址")]
    public string? URL { get; set; }

    [Comment("Html文件内容")]
    public byte[]? FileContent { get; set; }
}

