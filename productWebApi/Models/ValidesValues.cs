﻿
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;

namespace productWebApi.Models;
public class ValidesValues
{
    public ValidesValues()
    {

    }
    public ValidesValues(int dfID,int? ptid,int fromRawID,int rSrid,string data)
    {
        DFID = dfID;
        PTID = ptid;
        FromRawID = fromRawID;
        RSRID = rSrid;
        Data = data;
        Type = FieldType.SelectOrOptional;
    }
    public int ID { get; set; }

    [Comment("ProductTypeID")]
    public int? PTID { get; set; }

    [Comment("DataDefinitionsID")]
    public int DFID { get; set; }

    [Comment("ProductRawID")]
    public int FromRawID { get; set; }

    [Comment("HtmlID")]
    public int RSRID { get; set; }

    [Comment("values json")]
    public string Data { get; set; }

    [Comment("展示类型：文本，下拉框")]
    [DefaultValue(FieldType.TextORString)]   //SelectOrOptional
    public FieldType Type { get; set; }


    [Comment("必填，选填，建议")]
    public RequireType Rule { get; set; }
}
