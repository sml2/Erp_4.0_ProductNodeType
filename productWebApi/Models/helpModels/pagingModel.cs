﻿
using System.Linq;

namespace productWebApi.Models.helpModels;
public class pagingModel<T>
{
    public int TotalCount { get; set; }

    public int PageSize { get; set; }

    public int CurrentPage { get; set; }

    public int PageCount { get; set; }

    public IQueryable<T> Lists { get; set; }
}
