﻿
namespace productWebApi.Models.searchModels;
public class RealShipRawDetail
{
    public int RealShipRawID { get; set; }

    public int ID { get; set; }

    public string Type { get; set; }
   
    public int?  NodeLength { get; set; }
    public string? URL { get; set; }
}


public class RealShipRawAll
{
    public int ID { get; set; }

    public string Name { get; set; }

    public string? Area { get; set; }

    public string? URL { get; set; }

    public int? Count { get; set; }
}

