using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using productWebApi.Data;

var builder = WebApplication.CreateBuilder(args);

var vueSourceFolder = builder.Configuration.GetSection("Spa:SourcePath").Value;
var vueStaticFolder = builder.Configuration.GetSection("Spa:StaticPath").Value;
var port = builder.Configuration.GetSection("Spa:Port").Value;

builder.Services.AddLogging(log =>
{
    log.AddFilter("System", LogLevel.Warning);
    log.AddFilter("Microsoft", LogLevel.Warning);
    log.AddLog4Net("Cfg/log4net.Config");
});

builder.Services.AddControllers();

builder.Services.AddDbContext<ProductNodeTypeDBContext>();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "productWebApi", Version = "v1" });
});

builder.Services.AddSpaStaticFiles(configuration =>
{ configuration.RootPath = vueStaticFolder; });


var app = builder.Build();

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "productWebApi v1"));
}

app.UseStaticFiles();

// UseRouting 向中间件管道添加路由匹配。 中间件查看应用中定义的终结点集，并基于请求 UseRouting 选择最佳终结点匹配项。
app.UseRouting();

app.UseHttpsRedirection();

app.UseAuthorization();

//app.MapControllers();作用：将控制器操作终结点添加到 IEndpointRouteBuilder ，而不指定任何路由

//调用 MapControllerRoute 或 MapAreaControllerRoute 以映射 逆路由 控制器和 属性路由 控制器

//UseEndpoints和MapControllerRoute 用于创建单个路由



//一下路由被称为专用的传统路由，因为它是传统路由，而且是专用于特定操作的
app.UseEndpoints(endpoints =>
{
    //传统路由
    endpoints.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}"
    );
});




app.UseSpa(spa =>
{
    spa.Options.SourcePath = vueSourceFolder;
    if (app.Environment.IsDevelopment())
    {
        spa.UseProxyToSpaDevelopmentServer($"http://localhost:{port}");
    }
});

app.Run();
