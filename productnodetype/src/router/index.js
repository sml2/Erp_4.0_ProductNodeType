import Vue from 'vue'
import Router from 'vue-router'
import Platform from '@/components/Platform'
import Amazon from '@/components/Amazon/Amazon'
import Node from '@/components/Amazon/Node'
import Product from '@/components/Amazon/Product'

import Shopee from '@/components/Shopee/Shopee'
import Category from '@/components/Shopee/Category'


Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Platform',
            component: Platform,
        },
        {
            path: '/Amazon',
            name: 'Amazon',
            component: Amazon
        },
        {
            path: '/Shopee',
            name: 'Shopee',
            component: Shopee,
        },
        {
            path: '/Amazon/Node',
            name: 'Node',
            component: Node
        },
        {
            path: '/Amazon/Product',
            name: 'Product',
            component: Product
        },
        {
            path: '/Shopee/Category',
            name: 'Category',
            component: Category
        }
    ]
})
